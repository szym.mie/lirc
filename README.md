# lirc

## Simple NodeJS long polling IRC client and server

### Usage

Just run the server (index.js).
The server will open on port 3000.

Type in your messages.
And you're done.

Please note that server channels are non-existent.

#### System commands

*lirc* currently supports only a few of them:

- **topic** *[topic]* - list/change curent channel topic.
- **list** - list current users on this server.
- **priv** *[user]* *[text]* - send a private message to *user* with the message being *text*. You can only send a message to one user at the time.

### Bugs

Probably not. They are usually linked to the long-polling requests so the tend to self-fix at timeouts.

### The Long-Polling Timeouts

Different servers have varied max timeout times. For example *Herkou* has a 30 second limit - you will need to change a constant named *MAX_TTL* in *index.js* to make sure it's working properly.
