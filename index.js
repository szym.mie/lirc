const http = require("http");
const fs = require("fs");

const MAX_TTL = 15000;

function serve(req, res) {
    switch (req.method) {
        case "GET":
            page(req, res);
            break;

        case "POST":
            poll(req, res);
            break;

        default:
            break;
    }
}

function path(url) {
    return url.split('?')[0];
}

function params(path) {
    const params = path.split('?')[1].split('&');
    if (params) {
        const entries = {};
        for (const param of params) {
            const pair = decodeURIComponent(param).split('=');
            entries[pair[0]] = pair[1];
        }
        return entries;
    } else return '';
}

function page(req, res) {
    const path = req.url === '/' ? "./pages/index.html" : "./pages" + req.url;
    fs.readFile(path, function(err, buf) {
        if (!err) {
            res.writeHead(200);
            res.write(buf);
            res.end();
        } else {
            res.writeHead(404);
            res.end();
        }
    });
}

function poll(req, res) {
    const param = params(req.url);
    switch (path(req.url)) {
        case "/send":
            msg_broad(param);
            res.end();
            break;

        case "/recv":
            msg_wait(param, res);
            break;

        default:
            break;
    }
}

function msg_wait(param, res) {
    if (opened[param.name]) {
        clearTimeout(opened[param.name].ttl);
    }
    opened[param.name] = { res: res, ttl: setTimeout(
        function() { res.writeHead(408); res.end(); opened[param.name] = false; }, 
        MAX_TTL) };
}

function msg_broad(param) {
    const comm = command(param);

    const date = new Date();
    const hrs = date.getHours().toString().padStart(2, '0');
    const min = date.getMinutes().toString().padStart(2, '0');
    const msg = {
        date: hrs + ':' + min,
        name: comm ? "<s>PRIV</s>" : '@'+param.name,
        text: comm ? comm.text : param.text
    };

    console.log(param, Object.entries(opened).map(([k, v]) => k + ':' + !!v.res))
    if (!comm.name) {
        for (const name in opened) {
            const res = opened[name].res;
            if (res) {
                res.writeHead(200);
                res.write(JSON.stringify(msg));
                res.end();
                clearTimeout(opened[name].ttl);
                opened[name] = false;
            }
        }
    } else {
        const res = opened[comm.name].res;
        if (res) {
            res.writeHead(200);
            res.write(JSON.stringify(msg));
            res.end();
            clearTimeout(opened[comm.name].ttl);
            opened[comm.name] = false;
        }
    }

}

function command(param) {
    const args = param.text.split(' ');
    switch (args[0]) {
        case "/names":
            return { text: "<i>Names on server: " + names() + "</i>", name: param.name };
        
        case "/topic":
            if (args[1]) {
                topic = concat(args.slice(1));
                return { text: "<i>Current topic is: " + topic + "</i>", name: false };
            } else {
                return { text: "<i>Current topic is: " + topic + "</i>", name: param.name };
            }
        
        case "/priv":
            if (args[1] && args[2]) {
                return { text: "<i>&lt;@" + param.name + "&gt;</i> " + concat(args.slice(2)), name: args[1] };
            } else {
                return { text: "<i>Private message not sent.</i>", name: param.name };
            }
        
        default:
            return false;
    }
}

function names() {
    const names = [];
    for (const name in opened) {
        if (opened[name]) names.push(name);
    }
    return names;
}

function concat(texts) {
    let all = "";
    for (const text of texts) {
        all += text + ' ';
    }
    return all;
}

let topic = "Gdzie sa moje fajki?";
const opened = {};
const server = http.createServer(serve);
server.listen(process.env.PORT || 3000);